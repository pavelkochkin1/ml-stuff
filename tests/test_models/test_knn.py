from src.models.knn import find_neighbours
import numpy as np


def test_find_neighbours():
    data = np.array([[1, 2], [3, 4], [5, 6]])
    clusters = np.array([[1, 2], [3, 4]])
    res = [[0], [1], [1]]
    assert find_neighbours(k=1, X=clusters, points=data).tolist() == res
