from src.models.utils import distance
import numpy as np


def test_distance():
    data = np.array([[1, 2], [3, 4], [5, 6]])
    points = np.array([[1, 2], [3, 4]])
    res = [
        [0.0, 2.8284271247461903],
        [2.8284271247461903, 0.0],
        [5.656854249492381, 2.8284271247461903],
    ]
    assert distance(data, points).tolist() == res
