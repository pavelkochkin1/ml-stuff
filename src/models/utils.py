import numpy as np


def distance(
    data: np.ndarray,
    points: np.ndarray,
) -> np.ndarray:
    """Calculate distances from data to points.

    Args:
        data (np.ndarray): Some data. (N, M)
        points (np.ndarray): Points to which we want to calculate the distances. (K, M)

    Returns:
        np.ndarray: Distances from data to points. (N, K)
    """

    distances = np.empty((data.shape[0], points.shape[0]))
    for i, point in enumerate(points):
        distances[:, i] = np.sqrt(np.sum((data - point) ** 2, axis=1))
    return distances
