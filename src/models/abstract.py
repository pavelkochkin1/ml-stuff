from abc import ABCMeta, abstractmethod
import numpy as np


class SupervisedModel(metaclass=ABCMeta):
    """Abstract class for supervised models.
    This models can be connected with scikit-learn functions and transformers.

    Methods:
        fit (X, y): Fitting estimator.
        predict (X): Return predictions by the inserted data.
        score (X, y): Return metric score.
    """

    @abstractmethod
    def fit(self, X: np.ndarray, y: np.ndarray):
        pass

    @abstractmethod
    def predict(self, X: np.ndarray) -> np.ndarray:
        pass

    @abstractmethod
    def score(self, X: np.ndarray, y: np.ndarray) -> float:
        pass


class UnsupervisedModel(metaclass=ABCMeta):
    """Abstract class for unsupervised models.
    This models can be connected with scikit-learn functions and transformers.

    Methods:
        fit (X, y=0): Fitting estimator. Default y = 0 to connect with sklearn's
            pipelines and transformers.
        predict (X): Return predictions by the inserted data.
        score (X, y): Return metric score.
    """

    @abstractmethod
    def fit(self, X: np.ndarray, y=0):
        pass

    @abstractmethod
    def predict(self, X: np.ndarray) -> np.ndarray:
        pass

    @abstractmethod
    def score(self, X: np.ndarray) -> float:
        pass
