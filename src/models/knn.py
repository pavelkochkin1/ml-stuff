from src.models.abstract import SupervisedModel
import numpy as np
from sklearn.metrics import mean_squared_error, accuracy_score
from src.models.utils import distance
from collections import Counter


def find_neighbours(k: int, X: np.ndarray, points: np.ndarray) -> np.ndarray:
    """Find k indexes in the `X` array which are more close to the `points` than other.

    Args:
        k (int): How many indexes need to recieve.
        X (np.ndarray): An array in which we will find neighbours.
        points (np.ndarray): Points for which we will find neighbours.

    Returns:
        np.ndarray: Indexes of k neighbours.
    """
    distances = distance(points, X)
    idx = np.argpartition(distances, k)[:, :k]
    return idx


class KNNRegressor(SupervisedModel):
    """A regression model that makes predictions based on the neighbors of test points.

    Args:
        k_neighbours (int, optional): Count of neighbours to find. Defaults to 3.

    Methods:
        fit (X, y): Fitting estimator.
        predict (X): Return predictions by the inserted data.
            Mean value in the neighbours.
        score (X, y): Return metric score.
    """

    def __init__(self, k_neighbours: int = 3):
        """A regression model that makes predictions based
            on the neighbors of test points.

        Args:
            k_neighbours (int, optional): Count of neighbours to find. Defaults to 3.
        """
        self.k_neighbours = k_neighbours

    def fit(self, X: np.ndarray, y: np.ndarray) -> None:
        """Fitting estimator. Save X and y data to predict test points.

        Args:
            X (np.ndarray): source data. (N, M)
            y (np.ndarray): ground truth. (N, 1)
        """
        self.X = X
        self.y = y

    def predict(self, X: np.ndarray) -> np.ndarray:
        """Make predictions based on train_data and neighbours.

        Args:
            X (np.ndarray): Text data. (N, M)

        Returns:
            np.ndarray: array with predictions (N, 1)
        """
        k_neighbours = find_neighbours(self.k_neighbours, self.X, X)
        prediction: np.ndarray = np.mean(self.y[k_neighbours], axis=1)
        return prediction

    def score(self, X: np.ndarray, y: np.ndarray) -> float:
        """Return MSE metric.

        Args:
            X (np.ndarray): data
            y (np.ndarray): ground truth

        Returns:
            float: calculated metric
        """
        prediction = self.predict(X)
        metric: float = mean_squared_error(y, prediction, squared=False)
        return metric


class KNNClassifier(SupervisedModel):
    """A classification model that makes predictions
    based on the neighbors of test points.

    Args:
        k_neighbours (int, optional): Count of neighbours to find. Defaults to 3.

    Methods:
        fit (X, y): Fitting estimator.
        predict (X): Return predictions by the inserted data.
            Most common class in the neighbours.
        score (X, y): Return metric score.
    """

    def __init__(self, k_neighbours: int = 3):
        """A regression model that makes predictions based
            on the neighbors of test points.

        Args:
            k_neighbours (int, optional): Count of neighbours to find. Defaults to 3.
        """
        self.k_neighbours = k_neighbours

    def fit(self, X, y):
        """Fitting estimator. Save X and y data to predict test points.

        Args:
            X (np.ndarray): source data. (N, M)
            y (np.ndarray): ground truth. (N, 1)
        """
        self.X = X
        self.y = y

    def predict(self, X: np.ndarray) -> np.ndarray:
        """Make predictions based on train_data and neighbours.

        Args:
            X (np.ndarray): Text data. (N, M)

        Returns:
            np.ndarray: array with predictions (N, 1)
        """
        k_neighbours = self.y[find_neighbours(self.k_neighbours, self.X, X)]
        labels = [Counter(nbr).most_common(1)[0][0] for nbr in k_neighbours]
        return np.array(labels)

    def score(self, X: np.ndarray, y: np.ndarray) -> float:
        """Return accuracy score.

        Args:
            X (np.ndarray): data
            y (np.ndarray): ground truth

        Returns:
            float: calculated metric
        """
        prediction = self.predict(X)
        metric: float = accuracy_score(y, prediction)
        return metric
