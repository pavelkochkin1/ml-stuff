import warnings
from typing import Iterable

import numpy as np
from src.models.abstract import UnsupervisedModel
from src.models.utils import distance
from tqdm import trange

warnings.simplefilter("ignore", category=RuntimeWarning)


class KMeans(UnsupervisedModel):
    """_summary_

    Args:
        UnsupervisedModel (_type_): _description_
    """

    def __init__(
        self,
        n_clusters: int = 3,
        max_iter: int = 100,
        tol: float = 1e-4,
        verbose: bool = False,
        random_state: int = None,
    ) -> None:
        """Model for clustering using distances in samples.

        Args:
            n_clusters (int, optional): Number of clusters. Defaults to 3.
            max_iter (int, optional): Maximum number of iteration. Defaults to 100.
            tol (float, optional): Cluster indentation for early stopping
                of the algorithm. Defaults to 1e-4.
            verbose (bool, optional): Show progressbar. Defaults to False.
            random_state (int, optional): Seed of random for reproducability.
                Defaults to None.
        """

        self.n_clusters = n_clusters
        self.tol = tol
        self.verbose = verbose
        self.max_iter = max_iter
        self.random_state = random_state

    def fit(self, X: np.ndarray, y=0) -> None:
        """Fitting estimator.
        Find optimal centers for N clusters.

        Args:
            X (np.ndarray): source data. (N, M)
            y: To avoid problems with connection to sklearn. Default to 0.
        """

        if self.random_state is not None:
            np.random.seed(self.random_state)

        idx = np.random.choice(len(X), self.n_clusters, replace=False)
        self.centroids = X[idx, :]
        distances = distance(X, self.centroids)
        points = np.array([np.argmin(i) for i in distances])

        if self.verbose:
            tqdm_range = trange(self.max_iter, desc="KMeans: ", leave=True)
        else:
            tqdm_range = range(self.max_iter)

        self._optimal_centroids(X, points, tqdm_range)

    def predict(self, X: np.ndarray) -> np.ndarray:
        """Makes a prediction based on the distance to the nearest cluster.

        Args:
            X (np.ndarray): Text data. (N, M)

        Returns:
            np.ndarray: array with predictions (N, 1)
        """
        distances = distance(X, self.centroids)
        points = np.array([np.argmin(i) for i in distances])

        return points

    def score(self, X: np.ndarray) -> float:
        """Calculate sum of squared distances to nearest clusters and
        divide this on shape of data.

        Args:
            X (np.ndarray): data

        Returns:
            float: array with predicted clusters.
        """

        distances = distance(X, self.centroids)
        sqr_dist = np.min(distances, axis=1) ** 2
        interia: float = np.sum(sqr_dist) / X.shape[0]
        return interia

    def _optimal_centroids(
        self,
        X: np.ndarray,
        init_points: np.ndarray,
        kmrange: Iterable,
    ):
        """Find optimal centers for N clusters.

        Args:
            X (np.ndarray): source data
            init_points (np.ndarray): initial cluster points
            kmrange (Iterable): Range-like object.
        """

        for _ in kmrange:
            new_centroids = list()
            for i in range(self.n_clusters):
                temp_cent = X[init_points == i].mean(axis=0)
                new_centroids.append(temp_cent)

            if np.abs(np.sum(self.centroids - new_centroids)) < self.tol:
                if self.verbose:
                    print("The centroids converged faster. Stop iterations.")
                self.interia_ = self.score(X)
                break

            self.centroids = np.vstack(new_centroids)

            distances = distance(X, self.centroids)
            init_points = np.array([np.argmin(i) for i in distances])

            self.interia_ = self.score(X)
